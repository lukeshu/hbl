Hobo Lisp
---------
> A Horrible Bad Lisp.  It doesn't want to stay in one place too long,
> and is eager to jump on whichever train is going where it needs.

<!--
  Copyright (C) 2024  Luke T. Shumaker <lukeshu@lukeshu.com>
  SPDX-License-Identifier: AGPL-3.0-or-later
  -->

Hobo Lisp is a Lisp dialect that is intended to lower (compile) to
other high-level programming languages.  The initial targets are Go
and Rust (but Guile and Xtensa assembly also tickle my fancy).

## Goals

Design goals:

 1. 90% of the code I type is basic control structures; let me write
    those `if` blocks and `for` loops in a syntax that I can share
    between my Go codebases and my Rust codebases and my … codebases.

 2. Easy to re-target to new outputs.

 3. The output is human-friendly, not like most generated code.  Given
    Hobo Lisb that I wrote, it should output the same Go that I'd
    write.  If you decide you don't want Hobo Lisp anymore, it should
    be easy to drop it; simply check its output, and start hacking on
    that instead.

 4. Building on goal 3, it must be able to translate idioms and
    conventions.  For example, a Lisp function named `some-predicate?`
    would be translated to a Go function named `isSomePredicate`; the
    `?`-suffix naming convention is translated into the `is`-prefix
    naming convention, and the `kebab-case` naming convention is
    translated into the `camelCase` naming convention.

Design anti-goals (things I'm against):

 1. Its own runtime library.  If I were writing Go directly (see goal
    3), I wouldn't use some "libhbl".

Design non-goals (not goals, but I'm not against them):

 1. Type-checking in the compiler.  It's fine to leverage the
    type-checker of the output language's compiler.

 2. A Repl.  I have no desire to implement runtime, much less one that
    could support a repl.

 3. Closeness to any particular other Lisp.  (Though I think the vibe
    is that it will be Scheme-like.  And I think taking inspiration
    from [Carp][] would be good.)

## Decisions

 - The file extension is `.hbl`.

 - `.hbl` files must be encoded as UTF-8.

 - There is no distinction between lists and vectors.  In most Lisps,
   vectors exist primarily as a performance optimization over the
   linked-list implementation of lists.  Because Hobo Lisp compiles to
   other languages, which mostly don't use linked-lists ubiquitously,
   the optimization would be superflous in Hobo Lisp.

## References / prior art

- [Carp][] is a typed Lisp with Rust-like memory management that
  compiles to C.

  The C is not human-like C.  For example, the following Carp:

  ```lisp
  (defn start-with-empty []
    (let [a {}
          b (Map.put a "aha" &123)]
      (println* (Map.get &b "aha"))))
  ```

  compiles to the following C:

  ```c
  void start_MINUS_with_MINUS_empty() {
      /* let */ {
          Array _6 = { .len = 0, .capacity = 0, .data = CARP_MALLOC(sizeof(Pair__String_int) * 0) };
          Map__String_int _7 = Map_from_MINUS_array__String_int(_6);
          Map__String_int a = _7;
          static String _11 = "aha";
          String *_11_ref = &_11;
          static int _14_lit = 123;
          int* _14 = &_14_lit; // ref
          Map__String_int _15 = Map_put__String_int(a, _11_ref, _14);
          Map__String_int b = _15;
          Map__String_int* _23 = &b; // ref
          static String _24 = "aha";
          String *_24_ref = &_24;
          int _25 = Map_get__String_int(_23, _24_ref);
          String _26 = Int_str(_25);
          String* _27 = &_26; // ref
          IO_println(_27);
          Map_delete__String_int(b);
          String_delete(_26);
      }
  }
  ```

  According to the goals of Hobo Lisp, it should compile to the
  following Go:

  ```go
  func startWithEmpty() {
  	b := map[string]int{
  		"aha": 123,
  	}
  	fmt.Println(b["aha"])
  }
  ```

[Carp]: https://github.com/carp-lang/Carp
