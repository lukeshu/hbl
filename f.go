package example

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func println(args ...any) {
	fmt.Println(args...)
}

func InternalServerError(resp http.ResponseWriter, req *http.Request) {
	resp.Header()["Allow-Origin"] = req.Header["Origin"]
	resp.Header()["Content-Type"] = []string{"application/json"}
	resp.WriteHeader(500)
	json.NewEncoder(resp).Encode(map[string]any{
		"code":    500,
		"message": "internal server error",
	})
}
