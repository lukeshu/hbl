# Copyright (C) 2023-2024  Umorpha Systems
# Copyright (C) 2024  Luke T. Shumaker <lukeshu@lukeshu.com>
# SPDX-License-Identifier: AGPL-3.0-or-later

# Intro ########################################################################

all: build
.PHONY: all

.DELETE_ON_ERROR:
.NOTINTERMEDIATE:

FORCE:
.PHONY: FORCE

# Generate #####################################################################

generate/files = COPYING.txt
COPYING.txt:
	curl https://www.gnu.org/licenses/agpl-3.0.txt >$@

generate: $(generate/files)
generate-clean:
	rm -f -- $(generate/files)
.PHONY: generate generate-clean

# Build ########################################################################

build/files = out-bin/go/hbl

build: generate $(build/files)
clean:
	rm -rf out-bin
.PHONY: build clean

out-bin/go/.hbl.stamp: out-src/go FORCE
	cd $< && go build -o $(abspath $@)
out-bin/go/hbl: out-bin/go/%: out-bin/go/.%.stamp
	if cmp -s $< $@; then echo $@: already up to date; else PS4=; set -x; cp -f $< $@; fi
