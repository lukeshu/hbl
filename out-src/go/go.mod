// Copyright (C) 2024  Luke T. Shumaker <lukeshu@lukeshu.com>
// SPDX-License-Identifier: AGPL-3.0-or-later

module gitlab.com/lukeshu/hbl

go 1.21.6

require github.com/spf13/pflag v1.0.5
