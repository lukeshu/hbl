// Copyright (C) 2023  Umorpha Systems
// Copyright (C) 2024  Luke T. Shumaker <lukeshu@lukeshu.com>
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
"strings"
	"fmt"
	"io"
	"os"
	"errors"
	"io/fs"
	"path/filepath"

	"github.com/spf13/pflag"
)

// Standard exit codes defined by [LSB].
//
// [LSB]: http://refspecs.linuxbase.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/iniscrptact.html
const (
	ExitFailure         = 1
	ExitInvalidArgument = 2
)

type ErrorWithCode struct {
	Err  error
	Code int
}

// Error implements the [error] interface.
func (e *ErrorWithCode) Error() string { return e.Err.Error() }

// Unwrap implements the interface for [errors.Unwrap].
func (e *ErrorWithCode) Unwrap() error { return e.Err }

func main() {
	if err := MainWithError(os.Args[1:]); err != nil {
		fmt.Fprintf(os.Stderr, "%s: error: %v\n", os.Args[0], err)
		var ecode *ErrorWithCode
		if errors.As(err, &ecode) {
			if ecode.Code == ExitInvalidArgument {
				fmt.Fprintf(os.Stderr, "Try '%s --help' for more information.\n", os.Args[0])
			}
			os.Exit(ecode.Code)
		}
		os.Exit(ExitFailure)
	}

}

func usage(argparser *pflag.FlagSet) {
	fmt.Printf(`Usage: %s [OPTIONS] SOURCES...
Hobo Lisp to Go compiler

OPTIONS:
`, argparser.Name())
	argparser.SetOutput(os.Stdout)
	argparser.PrintDefaults()
}

func MainWithError(args []string) error {
	argparser := pflag.NewFlagSet(os.Args[0], pflag.ContinueOnError)
	argHelp := argparser.Bool("help", false, "show this help text")
	argOutput := argparser.StringP("output", "o", "", "output destination")

	argparser.SetOutput(io.Discard)
	err := argparser.Parse(args)
	if *argHelp {
		usage(argparser)
		return nil
	}
	if err != nil {
		return &ErrorWithCode{
			Err:  err,
			Code: ExitInvalidArgument,
		}
	}
	if argparser.NArg() == 0 {
		return &ErrorWithCode{
			Err:  errors.New("expected at least 1 positional argument"),
			Code: ExitInvalidArgument,
		}
	}
	argSources := argparser.Args()

	////////////////////////////////////////////////////////////////////////

	var sourceFiles []string
	for _, source := range argSources {
		if err := filepath.Walk(source, func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}
			if ! strings.HasSuffix(info.Name(), ".hbl") {
				return nil
			}
			sourceFiles = append(sourceFiles, path)
			return nil
		}); err != nil {
			return err
		}
	}
	if len(sourceFiles) == 0 {
		return errors.New("no .hbl files")
	}

	return nil
}
